// Package remote contains server implementations that call
// corresponding methods on a remote server (by using the API client
// in the context).
package remote
