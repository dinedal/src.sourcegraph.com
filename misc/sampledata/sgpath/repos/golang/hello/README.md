# Go starter project

This is a Go starter project that demonstrates what Sourcegraph can do.

## Try it now

1. Hover over and click on the code in
   [hello.go](/sample/golang/hello/.tree/hello.go).
1. Look at the [CI test results](/sample/golang/hello/.builds/1) to
   see if this project's tests are passing. (Spoiler alert: they are!)
1. Check out live
   [godoc Go API documentation](/sample/golang/hello/.godoc) for this
   project.
1. Search for the `Name` func in the search bar at the top.

Now you're ready to start using Sourcegraph on your own
code. [Add your own projects](/) now!

*If you want, you can clone this repository, edit it, and push your
changes. You'll see them here immediately.*
