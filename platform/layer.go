package platform

// Layer represents a platform plugin point that enables an
// application to inject a display layer in the Sourcegraph code browser.
type Layer struct {
	// not yet implemented
}
