package changesets

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/rogpeppe/rog-go/parallel"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"sourcegraph.com/sqs/pbtypes"
	"src.sourcegraph.com/sourcegraph/go-sourcegraph/sourcegraph"
	"src.sourcegraph.com/sourcegraph/sgx/cli"

	"src.sourcegraph.com/sourcegraph/platform"
	"src.sourcegraph.com/sourcegraph/platform/putil"
	"src.sourcegraph.com/sourcegraph/util/tempedit"
	"src.sourcegraph.com/sourcegraph/util/timeutil"
)

const (
	srcChangesetCreateMsg = "SRC_CHANGESET_CREATEMSG"
	srcChangesetEditMsg   = "SRC_CHANGESET_EDITMSG"
)

type serveFlags struct {
	ReviewGuidelines string `long:"changesets.review-guidelines" description:"loads the given file as review guidelines and displays it on the changesets page (Markdown supported)."`
	JiraURL          string `long:"jira.url" description:"URL that hosts a JIRA instance."`
	JiraCredentials  string `long:"jira.credentials" description:"HTTP basic auth credentials in the form \"user:password\" for the specified JIRA instance."`
}

var flags serveFlags

func init() {
	cli.PostInit = append(cli.PostInit, func() {
		_, err := cli.Serve.AddGroup("Changesets", "Changesets", &flags)
		if err != nil {
			log.Fatal(err)
		}
	})

	changesetsGroup, err := platform.CLI.AddCommand("changeset",
		"manage changesets",
		"The changeset subcommands manage changesets.",
		&changesetsCmd{},
	)
	if err != nil {
		log.Fatal(err)
	}
	changesetsGroup.Aliases = []string{"changesets", "cs"}

	listC, err := changesetsGroup.AddCommand("list",
		"list changesets",
		"The `sgx changeset list` command lists changesets.",
		&changesetListCmd{},
	)
	if err != nil {
		log.Fatal(err)
	}
	listC.Aliases = []string{"ls"}

	_, err = changesetsGroup.AddCommand("create",
		"create a changeset",
		"The `sgx changeset create` command creates a new changeset.",
		&changesetCreateCmd{},
	)
	if err != nil {
		log.Fatal(err)
	}

	_, err = changesetsGroup.AddCommand("update",
		"update a changeset",
		"The `sgx changeset update` command updates a changeset.",
		&changesetUpdateCmd{},
	)
	if err != nil {
		log.Fatal(err)
	}

	_, err = changesetsGroup.AddCommand("merge",
		"merge a changeset",
		"The `sgx changeset merge` command merges a changeset into its base branch on the remote.",
		&changesetMergeCmd{},
	)
	if err != nil {
		log.Fatal(err)
	}

	_, err = changesetsGroup.AddCommand("close",
		"close a changeset",
		"The `sgx changeset close` command closes a changeset.",
		&changesetCloseCmd{},
	)
	if err != nil {
		log.Fatal(err)
	}

	_, err = changesetsGroup.AddCommand("open",
		"open a changeset",
		"The `sgx changeset open` command opens a changeset.",
		&changesetOpenCmd{},
	)
	if err != nil {
		log.Fatal(err)
	}

	_, err = changesetsGroup.AddCommand("review",
		"review a changeset",
		"The `sgx changeset review` command reviews a changeset.",
		&changesetReviewCmd{},
	)
	if err != nil {
		log.Fatal(err)
	}

	changesetsReviewersGroup, err := changesetsGroup.AddCommand("reviewers",
		"manage reviewers",
		"The changeset reviewers subcommands manage reviewers for a changeset.",
		&changesetsReviewersCmd{},
	)
	if err != nil {
		log.Fatal(err)
	}
	_, err = changesetsReviewersGroup.AddCommand("add",
		"add a reviewer to a changeset",
		"The `sgx changeset reviewers add` command adds a reviewer to a changeset.",
		&changesetReviewersAddCmd{},
	)
	if err != nil {
		log.Fatal(err)
	}
	_, err = changesetsReviewersGroup.AddCommand("remove",
		"remove a reviewer to a changeset",
		"The `sgx changeset reviewers remove` command removes a reviewer from a changeset.",
		&changesetReviewersRemoveCmd{},
	)
	if err != nil {
		log.Fatal(err)
	}
	_, err = changesetsReviewersGroup.AddCommand("list",
		"list reviewers on a changeset",
		"The `sgx changeset reviewers list` command lists all reviewers on a changeset.",
		&changesetReviewersListCmd{},
	)
	if err != nil {
		log.Fatal(err)
	}
}

type changesetsCmd struct{}

func (c *changesetsCmd) Execute(args []string) error { return nil }

type changesetsCmdCommon struct {
	// Use the Repo function instead. This ensures the repo exists
	RepoDONOTUSE string `short:"r" long:"repo" description:"repository URI"`
}

func (c *changesetsCmdCommon) Repo() (*sourcegraph.Repo, error) {
	var isGuess bool
	repoURI := c.RepoDONOTUSE
	if repoURI == "" {
		var err error
		isGuess = true
		repoURI, err = guessRepo()
		if err != nil {
			log.Printf("Could not guess repo due to error: %s", err)
		}
		if repoURI == "" {
			return nil, errors.New("Could not guess repo, please specify a repo with -r.")
		}
	}

	cliCtx := putil.CLIContext()
	sg, err := sourcegraph.NewClientFromContext(cliCtx)
	if err != nil {
		return nil, err
	}

	repo, err := sg.Repos.Get(cliCtx, &sourcegraph.RepoSpec{URI: repoURI})
	if err != nil && isGuess && grpc.Code(err) == codes.NotFound {
		return nil, errors.New("Could not guess repo, please specify a repo with -r.")
	}
	return repo, err
}

type changesetListCmd struct {
	changesetsCmdCommon
	Status string `long:"status" description:"filter 'open', 'closed', 'assigned' (to me) or 'all' changesets (default: open)"`
}

func (c *changesetListCmd) Execute(args []string) error {
	cliCtx := putil.CLIContext()
	sg, err := sourcegraph.NewClientFromContext(cliCtx)
	if err != nil {
		return err
	}

	repo, err := c.Repo()
	if err != nil {
		return err
	}

	var open, closed, assigned bool
	switch c.Status {
	case "open", "":
		open = true
	case "closed":
		closed = true
	case "assigned":
		assigned = true
	case "all":
		open, closed = true, true
	default:
		return fmt.Errorf("Unrecognized status filter %v. Please pick one of open, closed, assigned or all", c.Status)
	}

	for page := 1; ; page++ {
		op := &sourcegraph.ChangesetListOp{
			Repo:        repo.URI,
			Open:        open,
			Closed:      closed,
			ListOptions: sourcegraph.ListOptions{Page: int32(page)},
		}
		if assigned {
			authInfo, err := sg.Auth.Identify(cliCtx, &pbtypes.Void{})
			if err != nil {
				return err
			}
			op.NeedsReview = &sourcegraph.UserSpec{
				UID:    authInfo.UID,
				Login:  authInfo.Login,
				Domain: authInfo.Domain,
			}
		}

		changesets, err := sg.Changesets.List(cliCtx, op)

		if err != nil {
			return err
		}
		if len(changesets.Changesets) == 0 {
			if page == 1 {
				fmt.Println("no changesets for", repo.URI)
			}
			break
		}
		for _, changeset := range changesets.Changesets {
			var status string
			if changeset.ClosedAt == nil {
				status = "open"
			} else {
				status = "closed"
			}
			fmt.Printf("#%d\t%s\t@%- 10s\t%s...%s\t%s (created %s)\n", changeset.ID, status, changeset.Author.Login, changeset.DeltaSpec.Base.Rev, changeset.DeltaSpec.Head.Rev, changeset.Title, timeutil.TimeAgo(changeset.CreatedAt.Time()))
		}
	}
	return nil
}

type changesetCreateCmd struct {
	changesetsCmdCommon
	Base  string `long:"base" description:"base branch"`
	Head  string `long:"head" description:"head branch"`
	Title string `short:"t" long:"title" description:"title"`
}

func (c *changesetCreateCmd) Execute(args []string) error {
	cliCtx := putil.CLIContext()
	sg, err := sourcegraph.NewClientFromContext(cliCtx)
	if err != nil {
		return err
	}

	repo, err := c.Repo()
	if err != nil {
		return err
	}

	var (
		par         = parallel.NewRun(4)
		authInfo    *sourcegraph.AuthInfo
		messagePath string
	)
	par.Do(func() error {
		if c.Base == "" {
			c.Base = repo.DefaultBranch
		}
		if c.Base == "" {
			return errors.New("must specify --base (could not determine default branch for repo)")
		}
		if _, err := sg.Repos.GetCommit(cliCtx, &sourcegraph.RepoRevSpec{RepoSpec: repo.RepoSpec(), Rev: c.Base}); err != nil {
			return fmt.Errorf("checking that base branch %q exists on remote server: %s", c.Base, err)
		}
		return nil
	})
	par.Do(func() error {
		if c.Head == "" {
			currentBranch, _ := exec.Command("git", "rev-parse", "--abbrev-ref", "HEAD").Output()
			c.Head = strings.TrimSpace(string(currentBranch))
		}
		if c.Head == "" {
			return errors.New("must specify --head (could not determine current branch)")
		}
		remoteHeadCommit, err := sg.Repos.GetCommit(cliCtx, &sourcegraph.RepoRevSpec{RepoSpec: repo.RepoSpec(), Rev: c.Head})
		if err != nil {
			return fmt.Errorf("checking that head branch %q exists on remote server: %s (did you git push?)", c.Head, err)
		}

		// Convenience check that local head commit == remote head
		// commit. There are a lot of edge cases, so don't make this check
		// required.
		localHeadCommit, err := exec.Command("git", "rev-parse", c.Head).Output()
		if err != nil {
			log.Printf("warning: failed to check local head commit of branch %q: %s", c.Head, err)
		} else if lhc := strings.TrimSpace(string(localHeadCommit)); lhc != string(remoteHeadCommit.ID) {
			log.Printf("warning: local branch %q head commit is %s, remote is %s", c.Head, lhc, remoteHeadCommit.ID)
		}
		return nil
	})
	par.Do(func() error {
		// TODO(sqs): Move this author field logic to the server so the
		// client doesn't have to fill in all of these fields.
		var err error
		authInfo, err = sg.Auth.Identify(cliCtx, &pbtypes.Void{})
		return err
	})
	par.Do(func() error {
		messagePath = changesetMessagePath(srcChangesetCreateMsg)
		return nil
	})
	err = par.Wait()
	if err != nil {
		return err
	}

	if !authInfo.Write {
		return grpc.Errorf(codes.Unauthenticated, "You need to authenticate with a user account which has write permission")
	}

	title, description, err := newChangesetInEditor(c.Title, messagePath)
	if err != nil {
		return err
	}

	changeset, err := sg.Changesets.Create(cliCtx, &sourcegraph.ChangesetCreateOp{
		Repo: repo.RepoSpec(),
		Changeset: &sourcegraph.Changeset{
			Title:       title,
			Description: description,
			Author: sourcegraph.UserSpec{
				UID:    authInfo.UID,
				Login:  authInfo.Login,
				Domain: authInfo.Domain,
			},
			DeltaSpec: &sourcegraph.DeltaSpec{
				Base: sourcegraph.RepoRevSpec{RepoSpec: repo.RepoSpec(), Rev: c.Base},
				Head: sourcegraph.RepoRevSpec{RepoSpec: repo.RepoSpec(), Rev: c.Head},
			},
		},
	})
	if err != nil {
		return err
	}

	conf, err := sg.Meta.Config(cliCtx, &pbtypes.Void{})
	if err != nil {
		return err
	}
	baseURL, err := url.Parse(conf.AppURL)
	if err != nil {
		return err
	}
	relURL, err := urlToRepoChangeset(repo.URI, changeset.ID)
	if err != nil {
		return err
	}
	log.Println(baseURL.ResolveReference(&url.URL{Path: relURL.Path[1:]}))
	os.Remove(messagePath)
	return nil
}

func newChangesetInEditor(origTitle, path string) (title, description string, err error) {
	var contents string
	if b, err := ioutil.ReadFile(path); err != nil || os.IsNotExist(err) {
		contents = origTitle + `
# Please enter the changeset title (in the first line) and description
# (in the subsequent lines). Lines starting with '#' will be ignored,
# and an empty message aborts the changeset.
`
	} else if err != nil {
		return "", "", err
	} else {
		contents = fmt.Sprintf(`%s
# Found existing description at %s
`, string(b), path)
	}

	txt, err := tempedit.Edit([]byte(contents), path)
	if err != nil {
		return "", "", err
	}

	title, description = parseMessage(txt)
	if title == "" {
		return "", "", errors.New("aborting changeset due to empty title")
	}

	return
}

// parseMessage will take a message for a changeset and split it into the
// title and body section. This uses similar rules to git's commit message
// editor.
func parseMessage(txt []byte) (title string, description string) {
	scanner := bufio.NewScanner(bytes.NewBuffer(txt))
	for scanner.Scan() {
		line := scanner.Text()
		if strings.HasPrefix(line, "#") {
			continue
		}
		if title == "" {
			title = strings.TrimSpace(line)
		} else {
			description += line + "\n"
		}
	}
	return title, strings.TrimSpace(description)
}

func changesetMessagePath(name string) string {
	root, err := exec.Command("git", "rev-parse", "--show-toplevel").Output()
	if err != nil || len(root) == 0 {
		return ""
	}
	return filepath.Join(strings.TrimSpace(string(root)), ".git", name)
}

type changesetUpdateCmdCommon struct {
	changesetsCmdCommon
	Args struct {
		ID int64 `name:"ID" description:"changeset ID"`
	} `positional-args:"yes" required:"yes" count:"1"`
}

type changesetUpdateCmd struct {
	changesetUpdateCmdCommon
	Title       string `short:"t" long:"title" description:"new changeset title"`
	Description bool   `short:"d" long:"description" description:"update changeset description"`
}

func (c *changesetUpdateCmd) Execute(args []string) error {
	if c.Title == "" && !c.Description {
		return errors.New("must specify either --title or --description")
	}

	cliCtx := putil.CLIContext()
	sg, err := sourcegraph.NewClientFromContext(cliCtx)
	if err != nil {
		return err
	}

	repo, err := c.Repo()
	if err != nil {
		return err
	}

	cs, err := sg.Changesets.Get(cliCtx, &sourcegraph.ChangesetGetOp{
		Spec: sourcegraph.ChangesetSpec{
			Repo: repo.RepoSpec(),
			ID:   c.Args.ID,
		},
	})
	if err != nil {
		return err
	}

	newDescription, err := tempedit.Edit([]byte(cs.Description), changesetMessagePath(srcChangesetEditMsg))
	if err != nil {
		return err
	}

	_, err = sg.Changesets.Update(cliCtx, &sourcegraph.ChangesetUpdateOp{
		Repo:        repo.RepoSpec(),
		ID:          c.Args.ID,
		Title:       c.Title,
		Description: string(newDescription),
	})
	if err != nil {
		return err
	}

	log.Printf("# updated changeset %s #%d", repo.URI, c.Args.ID)
	return nil
}

type changesetMergeCmd struct {
	changesetsCmdCommon
	Squash bool `long:"squash" description:"squash multiple commits on head into a single merge commit"`
	Args   struct {
		ID int64 `name:"ID" description:"changeset ID"`
	} `positional-args:"yes" required:"yes" count:"1"`
}

func (c *changesetMergeCmd) Execute(args []string) error {
	cliCtx := putil.CLIContext()
	sg, err := sourcegraph.NewClientFromContext(cliCtx)
	if err != nil {
		return err
	}

	repo, err := c.Repo()
	if err != nil {
		return err
	}

	_, err = sg.Changesets.Merge(cliCtx, &sourcegraph.ChangesetMergeOp{
		Repo:   repo.RepoSpec(),
		ID:     c.Args.ID,
		Squash: c.Squash,
	})
	if err != nil {
		return errors.New(grpc.ErrorDesc(err))
	}

	log.Printf("# merged changeset %s #%d", repo.URI, c.Args.ID)
	return nil
}

type changesetCloseCmd struct{ changesetUpdateCmdCommon }

func (c *changesetCloseCmd) Execute(args []string) error {
	cliCtx := putil.CLIContext()
	sg, err := sourcegraph.NewClientFromContext(cliCtx)
	if err != nil {
		return err
	}

	repo, err := c.Repo()
	if err != nil {
		return err
	}

	ev, err := sg.Changesets.Update(cliCtx, &sourcegraph.ChangesetUpdateOp{
		Repo:  repo.RepoSpec(),
		ID:    c.Args.ID,
		Close: true,
	})
	if err != nil {
		return err
	}

	log.Printf("# closed changeset %s #%d", repo.URI, ev.After.ID)
	return nil
}

type changesetOpenCmd struct{ changesetUpdateCmdCommon }

func (c *changesetOpenCmd) Execute(args []string) error {
	cliCtx := putil.CLIContext()
	sg, err := sourcegraph.NewClientFromContext(cliCtx)
	if err != nil {
		return err
	}

	repo, err := c.Repo()
	if err != nil {
		return err
	}

	ev, err := sg.Changesets.Update(cliCtx, &sourcegraph.ChangesetUpdateOp{
		Repo: repo.RepoSpec(),
		ID:   c.Args.ID,
		Open: true,
	})
	if err != nil {
		return err
	}

	log.Printf("# opened changeset %s #%d", repo.URI, ev.After.ID)
	return nil
}

type changesetReviewCmd struct {
	changesetUpdateCmdCommon
	LGTM    bool `long:"lgtm" description:"mark the changeset as looks-good-to-me"`
	NotLGTM bool `long:"not-lgtm" description:"mark the changeset as not-LGTM"`
}

func (c *changesetReviewCmd) Execute(args []string) error {
	// Check if they didn't specify either flag, or if they specified both.
	if c.LGTM == c.NotLGTM {
		log.Fatal("Must specify one of --lgtm or --not-lgtm")
	}

	cliCtx := putil.CLIContext()
	sg, err := sourcegraph.NewClientFromContext(cliCtx)
	if err != nil {
		return err
	}

	repo, err := c.Repo()
	if err != nil {
		return err
	}

	// TODO(sqs): Move this author field logic to the server so the
	// client doesn't have to fill in all of these fields.
	authInfo, err := sg.Auth.Identify(cliCtx, &pbtypes.Void{})
	if err != nil {
		return err
	}
	if !authInfo.Write {
		return grpc.Errorf(codes.Unauthenticated, "You need to authenticate with a user account which has write permission")
	}
	author := sourcegraph.UserSpec{
		UID:    authInfo.UID,
		Login:  authInfo.Login,
		Domain: authInfo.Domain,
	}

	if c.LGTM {
		ev, err := sg.Changesets.Update(cliCtx, &sourcegraph.ChangesetUpdateOp{
			Repo:   repo.RepoSpec(),
			ID:     c.Args.ID,
			LGTM:   true,
			Author: author,
		})
		if err != nil {
			return err
		}
		log.Printf("# changeset LGTM %s #%d\n", repo.URI, ev.After.ID)
	} else if c.NotLGTM {
		ev, err := sg.Changesets.Update(cliCtx, &sourcegraph.ChangesetUpdateOp{
			Repo:    repo.RepoSpec(),
			ID:      c.Args.ID,
			NotLGTM: true,
			Author:  author,
		})
		if err != nil {
			return err
		}
		log.Printf("# changeset not-LGTM %s #%d\n", repo.URI, ev.After.ID)
	}
	return nil
}

type changesetsReviewersCmd struct{}

func (c *changesetsReviewersCmd) Execute(args []string) error { return nil }

type changesetReviewersAddCmd struct {
	changesetUpdateCmdCommon
	Args struct {
		Reviewer []string `name:"Reviewer" description:"Username of reviewer to add to the changeset"`
	} `positional-args:"yes" required:"yes"`
}

func (c *changesetReviewersAddCmd) Execute(args []string) error {
	cliCtx := putil.CLIContext()
	sg, err := sourcegraph.NewClientFromContext(cliCtx)
	if err != nil {
		return err
	}

	repo, err := c.Repo()
	if err != nil {
		return err
	}

	// TODO(sqs): Move this author field logic to the server so the
	// client doesn't have to fill in all of these fields.
	authInfo, err := sg.Auth.Identify(cliCtx, &pbtypes.Void{})
	if err != nil {
		return err
	}
	if !authInfo.Write {
		return grpc.Errorf(codes.Unauthenticated, "You need to authenticate with a user account which has write permission")
	}
	author := sourcegraph.UserSpec{
		UID:    authInfo.UID,
		Login:  authInfo.Login,
		Domain: authInfo.Domain,
	}

	for _, reviewer := range c.Args.Reviewer {
		ev, err := sg.Changesets.Update(cliCtx, &sourcegraph.ChangesetUpdateOp{
			Repo: repo.RepoSpec(),
			ID:   c.changesetUpdateCmdCommon.Args.ID,
			AddReviewer: &sourcegraph.UserSpec{
				Login: reviewer,
			},
			Author: author,
		})
		if err != nil {
			return err
		}
		log.Printf("# Added reviewer %q to changeset %s #%d\n", reviewer, repo.URI, ev.After.ID)
	}
	return nil
}

type changesetReviewersRemoveCmd struct {
	changesetUpdateCmdCommon
	Args struct {
		Reviewer []string `name:"Reviewer" description:"Username of reviewer to remove from the changeset"`
	} `positional-args:"yes" required:"yes"`
}

func (c *changesetReviewersRemoveCmd) Execute(args []string) error {
	cliCtx := putil.CLIContext()
	sg, err := sourcegraph.NewClientFromContext(cliCtx)
	if err != nil {
		return err
	}

	repo, err := c.Repo()
	if err != nil {
		return err
	}

	// TODO(sqs): Move this author field logic to the server so the
	// client doesn't have to fill in all of these fields.
	authInfo, err := sg.Auth.Identify(cliCtx, &pbtypes.Void{})
	if err != nil {
		return err
	}
	if !authInfo.Write {
		return grpc.Errorf(codes.Unauthenticated, "You need to authenticate with a user account which has write permission")
	}
	author := sourcegraph.UserSpec{
		UID:    authInfo.UID,
		Login:  authInfo.Login,
		Domain: authInfo.Domain,
	}

	for _, reviewer := range c.Args.Reviewer {
		ev, err := sg.Changesets.Update(cliCtx, &sourcegraph.ChangesetUpdateOp{
			Repo: repo.RepoSpec(),
			ID:   c.changesetUpdateCmdCommon.Args.ID,
			RemoveReviewer: &sourcegraph.UserSpec{
				Login: reviewer,
			},
			Author: author,
		})
		if err != nil {
			return err
		}
		log.Printf("# Removed reviewer %q from changeset %s #%d\n", reviewer, repo.URI, ev.After.ID)
	}
	return nil
}

type changesetReviewersListCmd struct{ changesetUpdateCmdCommon }

func (c *changesetReviewersListCmd) Execute(args []string) error {
	cliCtx := putil.CLIContext()
	sg, err := sourcegraph.NewClientFromContext(cliCtx)
	if err != nil {
		return err
	}

	repo, err := c.Repo()
	if err != nil {
		return err
	}

	cs, err := sg.Changesets.Get(cliCtx, &sourcegraph.ChangesetGetOp{
		Spec: sourcegraph.ChangesetSpec{
			Repo: repo.RepoSpec(),
			ID:   c.Args.ID,
		},
	})
	if err != nil {
		return err
	}
	if len(cs.Reviewers) == 0 {
		log.Printf("# No assigned reviewers on changeset %s #%d (add some with 'src changeset reviewers add <ID> <reviewer>')\n", repo.URI, c.Args.ID)
		return nil
	}
	for _, reviewer := range cs.Reviewers {
		lgtm := "LGTM"
		if !reviewer.LGTM {
			lgtm = "(not reviewed)"
		}
		log.Printf("# Reviewer %q - %s\n", reviewer.UserSpec.Login, lgtm)
	}
	return nil
}
