package platform

// CodeViewPanel is a platform plugin point panel that lets an app add custom
// content to the information panel displayed in the Sourcegraph code
// browser.
type CodeViewPanel struct {
	// not yet implemented
}
