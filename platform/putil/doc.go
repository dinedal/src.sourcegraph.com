// Package putil contains helper functions to be used by platform
// applications. This package should not have any heavyweight
// dependencies on packages in the main Sourcegraph repository. It
// should mainly depend on util packages inside the main Sourcegraph
// repository.
package putil
