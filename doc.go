// Package sourcegraph is the top-level package for Sourcegraph, the
// smarter code host for teams.
//
// See the README.md file
// (https://src.sourcegraph.com/sourcegraph/.tree/README.md) for more
// information.
package sourcegraph
