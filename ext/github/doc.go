// Package github implements GitHub integration (for access to
// repositories, users, orgs, etc.). It provides store type
// implementations, a GitHub discovery func, etc.
package github
