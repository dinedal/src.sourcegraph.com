package common

type State struct {
	BaseURI   string
	ReqPath   string
	CSRFToken string
}
