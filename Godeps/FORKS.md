# Godep forks

This file lists all forked repositories that are vendored in Godeps
under their original names. (For multi-package dependencies, it is
easier to keep a fork at the same import path than to rewrite the
import paths.)

* gopkg.in/gcfg.v1: github.com/sqs/gcfg branch `skip-unrecognized`
* github.com/spf13/hugo: github.com/sqs/hugo branch `vfs`
* github.com/drone/drone-exec: github.com/sourcegraph/drone-exec branch `dev`
