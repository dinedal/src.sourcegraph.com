// Package gogeneratedeps exists simply to allow godep to vendor certain packages needed for
// running `godep go generate` to generate code in this repository.
package gogeneratedeps
