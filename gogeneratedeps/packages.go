package gogeneratedeps

import "github.com/shurcooL/vfsgen"

// vfsgen is used for generating Go code that statically embeds files.
var _ = vfsgen.Generate
