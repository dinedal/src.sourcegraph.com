// Package discover implements the discovery process (to perform
// operations on federated repositories and other resources).
package discover
