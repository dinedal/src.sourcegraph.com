package local

import (
	"encoding/json"
	"log"
	pathpkg "path"
	"path/filepath"
	"time"

	"strings"

	"sort"

	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"gopkg.in/inconshreveable/log15.v2"
	"sourcegraph.com/sqs/pbtypes"
	app_router "src.sourcegraph.com/sourcegraph/app/router"
	authpkg "src.sourcegraph.com/sourcegraph/auth"
	"src.sourcegraph.com/sourcegraph/auth/authutil"
	"src.sourcegraph.com/sourcegraph/conf"
	"src.sourcegraph.com/sourcegraph/doc"
	"src.sourcegraph.com/sourcegraph/errcode"
	"src.sourcegraph.com/sourcegraph/ext/github"
	"src.sourcegraph.com/sourcegraph/go-sourcegraph/sourcegraph"
	"src.sourcegraph.com/sourcegraph/pkg/inventory"
	"src.sourcegraph.com/sourcegraph/pkg/vcs"
	"src.sourcegraph.com/sourcegraph/pkg/vfsutil"
	"src.sourcegraph.com/sourcegraph/platform"
	"src.sourcegraph.com/sourcegraph/repoupdater"
	"src.sourcegraph.com/sourcegraph/server/accesscontrol"
	localcli "src.sourcegraph.com/sourcegraph/server/local/cli"
	"src.sourcegraph.com/sourcegraph/store"
	"src.sourcegraph.com/sourcegraph/svc"
	"src.sourcegraph.com/sourcegraph/util/eventsutil"
)

var Repos sourcegraph.ReposServer = &repos{}

var errEmptyRepoURI = grpc.Errorf(codes.InvalidArgument, "repo URI is empty")
var errPermissionDenied = grpc.Errorf(codes.PermissionDenied, "cannot view this repo")

type repos struct{}

var _ sourcegraph.ReposServer = (*repos)(nil)

func (s *repos) Get(ctx context.Context, repo *sourcegraph.RepoSpec) (*sourcegraph.Repo, error) {
	r, err := s.get(ctx, repo.URI)
	if err != nil {
		return nil, err
	}
	if err := s.setRepoPermissions(ctx, r); err != nil {
		return nil, err
	}
	if err := s.setRepoOtherFields(ctx, r); err != nil {
		return nil, err
	}
	return r, nil
}

// get gets the repo from the store but does not fetch and populate
// the repo permissions. Callers that need the repo but not the
// permissions should call get (instead of Get) to avoid doing
// needless work.
func (s *repos) get(ctx context.Context, repo string) (*sourcegraph.Repo, error) {
	if repo == "" {
		return nil, errEmptyRepoURI
	}

	r, err := store.ReposFromContext(ctx).Get(ctx, repo)
	if err != nil {
		return nil, err
	}

	if r.Blocked {
		return nil, grpc.Errorf(codes.FailedPrecondition, "repo %s is blocked", repo)
	}

	return r, nil
}

func (s *repos) List(ctx context.Context, opt *sourcegraph.RepoListOptions) (*sourcegraph.RepoList, error) {
	repos, err := store.ReposFromContext(ctx).List(ctx, opt)
	if err != nil {
		return nil, err
	}
	if err := s.setRepoPermissions(ctx, repos...); err != nil {
		return nil, err
	}
	if err := s.setRepoOtherFields(ctx, repos...); err != nil {
		return nil, err
	}
	return &sourcegraph.RepoList{Repos: repos}, nil
}

// setRepoPermissions modifies repos in place, setting their
// Permissions fields by calling (store.Repos).GetPerms on each repo.
func (s *repos) setRepoPermissions(ctx context.Context, repos ...*sourcegraph.Repo) error {
	for _, repo := range repos {
		if repo.Permissions == nil {
			perms, err := store.ReposFromContext(ctx).GetPerms(ctx, repo.URI)
			if err != nil {
				return err
			}
			repo.Permissions = perms
		}
	}
	return nil
}

func (s *repos) setRepoOtherFields(ctx context.Context, repos ...*sourcegraph.Repo) error {
	appURL := conf.AppURL(ctx)
	for _, repo := range repos {
		repo.HTMLURL = appURL.ResolveReference(app_router.Rel.URLToRepo(repo.URI)).String()
	}
	return nil
}

func (s *repos) Create(ctx context.Context, op *sourcegraph.ReposCreateOp) (*sourcegraph.Repo, error) {
	if op.URI == "" {
		return nil, grpc.Errorf(codes.InvalidArgument, "repo URI must have at least one path component")
	}
	if op.Mirror {
		if op.CloneURL == "" {
			return nil, grpc.Errorf(codes.InvalidArgument, "creating a mirror repo requires a clone URL to be set")
		}
	}
	if _, err := s.get(elevatedActor(ctx), op.URI); err == nil {
		return nil, grpc.Errorf(codes.AlreadyExists, "repo already exists")
	}

	actor := authpkg.ActorFromContext(ctx)
	if authutil.ActiveFlags.PrivateMirrors {
		if !op.Mirror {
			// TODO: enable creating local repos on Sourcegraph Server instances.
			return nil, grpc.Errorf(codes.Unavailable, "creating a non-mirror repo is not enabled on this server")
		}

		if op.Private {
			// If this server has a waitlist in place, check that the user
			// is off the waitlist.
			if !actor.PrivateMirrors {
				return nil, grpc.Errorf(codes.PermissionDenied, "user is not allowed to create this repo")
			}

			// Check that the user has permission to access this private repo.
			// The user's permissions for private mirror repos are set in
			// MirrorRepos.GetUserData, which is called during onboarding.
			valid, err := store.RepoPermsFromContext(ctx).Get(elevatedActor(ctx), int32(actor.UID), op.URI)
			if err != nil {
				return nil, err
			}
			if !valid {
				return nil, grpc.Errorf(codes.PermissionDenied, "user is not allowed to create this repo")
			}
		} else {
			// Check that the repo is really public.
			ghRepos := github.Repos{}
			repo, err := ghRepos.Get(github.NewContextWithUnauthedClient(ctx), op.URI)
			if err != nil || repo.Private {
				log15.Warn("Could not fetch public GitHub repo in Repos.Create", "uri", op.URI, "error", err)
				return nil, grpc.Errorf(codes.PermissionDenied, "user is not allowed to create this repo")
			}
		}
	} else {
		if op.Mirror && op.Private {
			return nil, grpc.Errorf(codes.Unavailable, "creating a private mirror repo is not enabled on this server")
		}
	}
	ts := pbtypes.NewTimestamp(time.Now())
	repo := &sourcegraph.Repo{
		Name:         pathpkg.Base(op.URI),
		URI:          op.URI,
		VCS:          op.VCS,
		HTTPCloneURL: op.CloneURL,
		Mirror:       op.Mirror,
		Private:      op.Private,
		Description:  op.Description,
		Language:     op.Language,
		CreatedAt:    &ts,
	}

	if err := store.ReposFromContext(ctx).Create(elevatedActor(ctx), repo); err != nil {
		return nil, err
	}

	if op.Mirror {
		repoupdater.Enqueue(repo)
	}

	eventsutil.LogAddRepo(ctx, op.CloneURL, op.Language, op.Mirror, op.Private)

	accesscontrol.SetMirrorRepoPerms(ctx, &actor)
	ctx = authpkg.WithActor(ctx, actor)

	repoSpec := repo.RepoSpec()
	return s.Get(ctx, &repoSpec)
}

func (s *repos) Update(ctx context.Context, op *sourcegraph.ReposUpdateOp) (*sourcegraph.Repo, error) {
	ts := time.Now()
	update := &store.RepoUpdate{ReposUpdateOp: op, UpdatedAt: &ts}
	if err := store.ReposFromContext(ctx).Update(ctx, update); err != nil {
		return nil, err
	}
	return s.Get(ctx, &op.Repo)
}

func (s *repos) Delete(ctx context.Context, repo *sourcegraph.RepoSpec) (*pbtypes.Void, error) {
	if err := store.ReposFromContext(ctx).Delete(ctx, repo.URI); err != nil {
		return nil, err
	}
	return &pbtypes.Void{}, nil
}

// resolveRepoRev resolves repoRev to an absolute commit ID (by
// consulting its VCS data). If no rev is specified, the repo's
// default branch is used.
func (s *repos) resolveRepoRev(ctx context.Context, repoRev *sourcegraph.RepoRevSpec) error {
	if err := s.resolveRepoRevBranch(ctx, repoRev); err != nil {
		return err
	}

	if repoRev.CommitID == "" {
		vcsrepo, err := store.RepoVCSFromContext(ctx).Open(ctx, repoRev.URI)
		if err != nil {
			return err
		}
		commitID, err := vcsrepo.ResolveRevision(repoRev.Rev)
		if err != nil {
			return err
		}
		repoRev.CommitID = string(commitID)
	}

	return nil
}

func (s *repos) resolveRepoRevBranch(ctx context.Context, repoRev *sourcegraph.RepoRevSpec) error {
	if repoRev.CommitID == "" && repoRev.Rev == "" {
		// Get default branch.
		defBr, err := s.defaultBranch(ctx, repoRev.URI)
		if err != nil {
			return err
		}
		repoRev.Rev = defBr
	}

	const srclibRevTag = "^{srclib}" // REV^{srclib} refers to the newest srclib version from REV
	if strings.HasSuffix(repoRev.Rev, srclibRevTag) {
		origRev := repoRev.Rev
		repoRev.Rev = strings.TrimSuffix(repoRev.Rev, srclibRevTag)
		dataVer, err := svc.Repos(ctx).GetSrclibDataVersionForPath(ctx, &sourcegraph.TreeEntrySpec{RepoRev: *repoRev})
		if err == nil {
			// TODO(sqs): check this
			repoRev.CommitID = dataVer.CommitID
		} else if errcode.GRPC(err) == codes.NotFound {
			// Ignore NotFound as otherwise the user might not even be
			// able to access the repository homepage.
			log15.Warn("Failed to resolve to commit with srclib Code Intelligence data; will proceed by resolving to commit with no Code Intelligence data instead", "rev", origRev, "fallback", repoRev.Rev, "error", err)
		} else if err != nil {
			return grpc.Errorf(errcode.GRPC(err), "while resolving rev %q: %s", repoRev.Rev, err)
		}
	}

	return nil
}

func (s *repos) defaultBranch(ctx context.Context, repoURI string) (string, error) {
	repo, err := s.get(ctx, repoURI)
	if err != nil {
		return "", err
	}
	if repo.DefaultBranch == "" {
		return "", grpc.Errorf(codes.FailedPrecondition, "repo %s has no default branch", repoURI)
	}
	return repo.DefaultBranch, nil
}

func (s *repos) GetReadme(ctx context.Context, repoRev *sourcegraph.RepoRevSpec) (*sourcegraph.Readme, error) {
	if repoRev.URI == "" {
		return nil, errEmptyRepoURI
	}

	if err := s.resolveRepoRev(ctx, repoRev); err != nil {
		return nil, err
	}

	vcsrepo, err := store.RepoVCSFromContext(ctx).Open(ctx, repoRev.URI)
	if err != nil {
		return nil, err
	}

	commit := vcs.CommitID(repoRev.CommitID)

	entries, err := vcsrepo.ReadDir(commit, ".", false)
	if err != nil {
		return nil, err
	}

	filenames := make([]string, len(entries))
	for i, e := range entries {
		filenames[i] = e.Name()
	}

	readme := &sourcegraph.Readme{Path: doc.ChooseReadme(filenames)}
	if readme.Path == "" {
		return nil, grpc.Errorf(codes.NotFound, "no README found in %v", repoRev)
	}

	data, err := vcsrepo.ReadFile(commit, readme.Path)
	if err != nil {
		return nil, err
	}

	formatted, err := doc.ToHTML(doc.Format(readme.Path), data)
	if err != nil {
		log.Printf("Warning: doc.ToHTML on readme %q in repo %s failed: %s.", readme.Path, repoRev.URI, err)
	}
	readme.HTML = string(formatted)
	return readme, nil
}

func (s *repos) GetConfig(ctx context.Context, repo *sourcegraph.RepoSpec) (*sourcegraph.RepoConfig, error) {
	repoConfigsStore := store.RepoConfigsFromContext(ctx)

	conf, err := repoConfigsStore.Get(ctx, repo.URI)
	if err != nil {
		return nil, err
	}
	if conf == nil {
		conf = &sourcegraph.RepoConfig{}
	}
	return conf, nil
}

func (s *repos) ConfigureApp(ctx context.Context, op *sourcegraph.RepoConfigureAppOp) (*pbtypes.Void, error) {
	store := store.RepoConfigsFromContext(ctx)

	if op.Enable {
		// Check that app ID is a valid app. Allow disabling invalid
		// apps so that obsolete apps can always be removed.
		if _, present := platform.Frames()[op.App]; !present {
			return nil, grpc.Errorf(codes.InvalidArgument, "app %q is not a valid app ID", op.App)
		}
	}

	conf, err := store.Get(ctx, op.Repo.URI)
	if err != nil {
		return nil, err
	}
	if conf == nil {
		conf = &sourcegraph.RepoConfig{}
	}

	// Make apps list unique and add/remove the new app.
	apps := make(map[string]struct{}, len(conf.Apps))
	for _, app := range conf.Apps {
		apps[app] = struct{}{}
	}
	if op.Enable {
		apps[op.App] = struct{}{}
	} else {
		delete(apps, op.App)
	}
	conf.Apps = make([]string, 0, len(apps))
	for app := range apps {
		conf.Apps = append(conf.Apps, app)
	}
	sort.Strings(conf.Apps)

	if err := store.Update(ctx, op.Repo.URI, *conf); err != nil {
		return nil, err
	}
	return &pbtypes.Void{}, nil
}

func (s *repos) GetInventory(ctx context.Context, repoRev *sourcegraph.RepoRevSpec) (*inventory.Inventory, error) {
	if localcli.Flags.DisableRepoInventory {
		return nil, grpc.Errorf(codes.Unimplemented, "repo inventory listing is disabled by the configuration (DisableRepoInventory/--local.disable-repo-inventory)")
	}

	if err := s.resolveRepoRev(ctx, repoRev); err != nil {
		return nil, err
	}

	// Consult the commit status "cache" for a cached inventory result.
	//
	// We cache inventory result on the commit status. This lets us
	// populate the cache by calling this method from anywhere (e.g.,
	// after a git push). Just using the memory cache would mean that
	// each server process would have to recompute this result.
	const statusContext = "cache:repo.inventory"
	statusRev := sourcegraph.RepoRevSpec{RepoSpec: repoRev.RepoSpec, CommitID: repoRev.CommitID}
	statuses, err := svc.RepoStatuses(ctx).GetCombined(ctx, &statusRev)
	if err != nil {
		return nil, err
	}
	if status := statuses.GetStatus(statusContext); status != nil {
		var inv inventory.Inventory
		if err := json.Unmarshal([]byte(status.Description), &inv); err == nil {
			return &inv, nil
		}
		log15.Warn("Repos.GetInventory failed to unmarshal cached JSON inventory", "repoRev", statusRev, "err", err)
	}

	// Not found in the cache, so compute it.
	inv, err := s.getInventoryUncached(ctx, repoRev)
	if err != nil {
		return nil, err
	}

	// Store inventory in cache.
	jsonData, err := json.Marshal(inv)
	if err != nil {
		return nil, err
	}
	_, err = svc.RepoStatuses(ctx).Create(ctx, &sourcegraph.RepoStatusesCreateOp{
		Repo:   statusRev,
		Status: sourcegraph.RepoStatus{Description: string(jsonData), Context: statusContext},
	})
	if err != nil {
		return nil, err
	}

	return inv, nil
}

func (s *repos) getInventoryUncached(ctx context.Context, repoRev *sourcegraph.RepoRevSpec) (*inventory.Inventory, error) {
	vcsrepo, err := store.RepoVCSFromContext(ctx).Open(ctx, repoRev.URI)
	if err != nil {
		return nil, err
	}

	fs := vcs.FileSystem(vcsrepo, vcs.CommitID(repoRev.CommitID))
	inv, err := inventory.Scan(ctx, vfsutil.Walkable(fs, filepath.Join))
	if err != nil {
		return nil, err
	}
	return inv, nil
}

func (s *repos) verifyScopeHasPrivateRepoAccess(scope map[string]bool) bool {
	for k := range scope {
		if strings.HasPrefix(k, "internal:") {
			return true
		}
	}
	return false
}
