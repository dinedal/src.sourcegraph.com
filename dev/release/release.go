// Package release contains release-related constants.
package release

const (
	S3Bucket = "sourcegraph-release"
	S3Dir    = "src"
)
