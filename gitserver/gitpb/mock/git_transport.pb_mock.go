// generated by gen-mocks; DO NOT EDIT

package mock

import (
	"golang.org/x/net/context"
	"google.golang.org/grpc"
	"src.sourcegraph.com/sourcegraph/gitserver/gitpb"
)

type GitTransportClient struct {
	InfoRefs_    func(ctx context.Context, in *gitpb.InfoRefsOp) (*gitpb.Packet, error)
	ReceivePack_ func(ctx context.Context, in *gitpb.ReceivePackOp) (*gitpb.Packet, error)
	UploadPack_  func(ctx context.Context, in *gitpb.UploadPackOp) (*gitpb.Packet, error)
}

func (s *GitTransportClient) InfoRefs(ctx context.Context, in *gitpb.InfoRefsOp, opts ...grpc.CallOption) (*gitpb.Packet, error) {
	return s.InfoRefs_(ctx, in)
}

func (s *GitTransportClient) ReceivePack(ctx context.Context, in *gitpb.ReceivePackOp, opts ...grpc.CallOption) (*gitpb.Packet, error) {
	return s.ReceivePack_(ctx, in)
}

func (s *GitTransportClient) UploadPack(ctx context.Context, in *gitpb.UploadPackOp, opts ...grpc.CallOption) (*gitpb.Packet, error) {
	return s.UploadPack_(ctx, in)
}

var _ gitpb.GitTransportClient = (*GitTransportClient)(nil)

type GitTransportServer struct {
	InfoRefs_    func(v0 context.Context, v1 *gitpb.InfoRefsOp) (*gitpb.Packet, error)
	ReceivePack_ func(v0 context.Context, v1 *gitpb.ReceivePackOp) (*gitpb.Packet, error)
	UploadPack_  func(v0 context.Context, v1 *gitpb.UploadPackOp) (*gitpb.Packet, error)
}

func (s *GitTransportServer) InfoRefs(v0 context.Context, v1 *gitpb.InfoRefsOp) (*gitpb.Packet, error) {
	return s.InfoRefs_(v0, v1)
}

func (s *GitTransportServer) ReceivePack(v0 context.Context, v1 *gitpb.ReceivePackOp) (*gitpb.Packet, error) {
	return s.ReceivePack_(v0, v1)
}

func (s *GitTransportServer) UploadPack(v0 context.Context, v1 *gitpb.UploadPackOp) (*gitpb.Packet, error) {
	return s.UploadPack_(v0, v1)
}

var _ gitpb.GitTransportServer = (*GitTransportServer)(nil)
