// Package localauth adds local authentication handlers to the app,
// for signing up, logging in, resetting passwords, etc.
//
// In this context, "local" means "not via OAuth2."
package localauth
