// Package oauth2server adds OAuth2 authorization server handlers to
// the app to allow OAuth2 clients to authenticate users using this
// app.
//
// Status: It is intended to adhere to the OAuth2 standard but it has
// not yet been rigorously reviewed and tested.
package oauth2server
