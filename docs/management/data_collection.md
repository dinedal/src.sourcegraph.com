+++
title = "Data collection"
+++

Sourcegraph gives your team the power to build better software. When
you use Sourcegraph and share information with us, you help us make
Sourcegraph better. We want you to understand what information
Sourcegraph collects, why we collect it, and the choices we offer.

As a sign of our commitment to security and transparency, we have made
[Sourcegraph's source code](https://src.sourcegraph.com/sourcegraph)
publicly available.


# Information we collect

We believe that you should be able to host your code securely on your
own infrastructure *and* enjoy the benefits of a cloud service: easy
setup, seamless access to open-source code, continuous product
enhancements, and data-driven intelligence. Sourcegraph collects
limited information to provide these benefits while keeping you in
full control of your code.

We collect the following types of information to make Sourcegraph
better.

* **Information you give us:** For example, when you sign up for an
  account on Sourcegraph.com or set up a new Sourcegraph server, we
  ask for information such as your email address.
* **Information we get from your use of Sourcegraph:** To give you the
  best and most reliable features, we collect statistics about how you
  use Sourcegraph. This consists of metadata about user and server
  actions, such as pageview URLs and API method timings.
* **Information we collect from your repositories:** To give you the
  most intelligent developer platform, we use machine learning to
  automatically detect common bugs, alert you about security issues,
  and present opportunities to improve your code based on best
  practices. To provide these features, your server computes
  statistics and signals derived from code and sends aggregate
  information to us; none of your code is ever sent. We always invite
  you to
  [inspect the code for this functionality](https://src.sourcegraph.com/sourcegraph@master/.tree/util/statsutil/basic_stats.go).

Sourcegraph keeps your code on your own infrastructure.

See the full [security policy](https://sourcegraph.com/security),
[privacy policy](https://sourcegraph.com/privacy) and
[terms of service](https://sourcegraph.com/terms) for more
information.


# Choices

For companies whose policies prohibit the use of cloud services,
Sourcegraph Enterprise can operate without any external network
access. [Contact us](mailto:help@sourcegraph.com) for more
information.


