+++
title = "Reporting bugs"
+++

# Reporting Bugs

If you find bugs or documentation mistakes in Sourcegraph, please let us know by
[opening a thread](https://src.sourcegraph.com/sourcegraph/.tracker/new). We appreciate all
bug reports, no matter how small.

You can also open a thread while browsing through code; hover-over the left gutter
to open a thread which automatically references the code you're filing a report about.

To make your bug report accurate and easy to understand, please try to create bug reports that are:

- Specific. Include as much details as possible: which version, what environment, what configuration, etc. You can also attach logs. Whatever you think is relevant.

- Reproducible. Include the steps to reproduce the problem. We understand some issues might be hard to reproduce, so include the steps that might lead to the problem.

- Isolated. Please try to isolate and reproduce the bug with minimum dependencies. It would significantly slow down the speed to fix a bug if too many dependencies are involved in a bug report. Debugging external systems is out of scope, but we are happy to point you in the right direction or help you interact with Sourcegraph in the correct manner.

- Unique. Do not duplicate existing bug report.

See our [troubleshooting FAQ]{{< relref "troubleshooting/faq.md" >}} for useful debugging information.
