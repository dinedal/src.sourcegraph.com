+++
title = "Building the source code"
linktitle = "Building source"
+++

See the [README.dev.md](https://src.sourcegraph.com/sourcegraph@master/.tree/README.dev.md)
file in the Sourcegraph repository for instructions on installing Sourcegraph from source code.

{{< ads_conversion >}}
